import GetinTouch from "@/components/GetinTouch";
import Image from "next/image";
import styles from "../style.module.scss";
import Link from "next/link";
import { SlArrowLeft } from "react-icons/sl";

const Page = () => {
  return (
    <div className="flex flex-col items-center px-10 gap-10">
      <Image
        src="/MorT1.png"
        alt="MORT1"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={styles.title}>
            <h1>Monday or Tuesday</h1>
          </div>
          <p className={styles.tp}>Print Design</p>
        </div>
        <p className={styles.p}>
          A redesign of the book &quot;Monday or Tuesday&quot; by Virginia
          Woolf. The book encompasses short stories with various perspectives
          and underlining meanings. The design concept was to properly express
          and denote the different perspectives of the narrators, characters,
          and objects through metaphors. I have created these underlining themes
          using photographs, typography and layout styles.
        </p>
      </div>
      <Image
        src="/MorT1.png"
        alt="MORT1"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Screen Shot 2020-10-12 at 12.20.19 AM.png"
        alt="train book"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Screen Shot 2020-10-12 at 12.17.24 AM.png"
        alt="book"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/MondayorTuesday_StyleGuide.png"
        alt="style guide"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Screen Shot 2020-10-12 at 12.17.35 AM.png"
        alt="book"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className="flex justify-between w-full pt-20">
        <Link href="/salon-nuna" className="flex flex-row items-center gap-3">
          <SlArrowLeft className="text-2xl " />
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            Salon Nuna
          </p>
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
