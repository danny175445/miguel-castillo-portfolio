import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";
import styles from "../style.module.scss";
import Link from "next/link";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";

const Page = () => {
  return (
    <div className="flex flex-col items-center px-10 gap-10">
      <Image
        src="/Squarespace_FRN1.png"
        alt="frn1"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={styles.title}>
            <h1>Family Resource Navigators</h1>
          </div>
          <p className={styles.tp}>
            {" "}
            Visual Identity // Branding // Logo Design
          </p>
        </div>
        <p className={styles.p}>
          Family Resource Navigators (FRN) is a bay area based non-profit
          serving children or youth with disabilities or special health care
          needs in Alameda County. As their Contracted Visual Designer I was
          tasked with redesigning their logo and rejuvenating their color
          palette. The goal was to create a modern, warm, and family-oriented
          visual identity that can represent FRN’s mission and services.
        </p>
      </div>
      <Image
        src="/HandDrawings.png"
        alt="handdrawing"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Screen Shot 2020-10-06 at 11.43.58 PM.png"
        alt="screen shot"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/FRN_LogoVersionsV2.png"
        alt="FRN_ logo versionv2"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/FRN_BigLogoVersions2.png"
        alt="FRN_ biglogo versionv2"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/FRN_StyleGuide2.png"
        alt="FRN_StyleGuide2"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Screen Shot 2020-10-07 at 1.13.43 AM.png"
        alt="screen shot"
        width={0}
        height={0}
      />
      <Image
        src="/Financials.png"
        alt="financials"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Stats1png.png"
        alt="Stats1"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Spread2.png"
        alt="spread2"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className="flex justify-between w-full pt-20 gap-[40%] max-mobile:gap-[5%] max-[355px]:ml-[-20px]">
        <Link
          href="/ohlone-collage"
          className="flex flex-row items-center gap-3"
        >
          <SlArrowLeft className="text-2xl " />
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            Ohlone College Multicultural Student Center
          </p>
        </Link>
        <Link href="/frcnca" className="flex flex-row items-center">
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            FRCNCA
          </p>
          <SlArrowRight className="text-2xl" />
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
