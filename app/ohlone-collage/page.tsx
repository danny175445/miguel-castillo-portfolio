import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";
import styles from "../style.module.scss";
import Link from "next/link";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";

const Page = () => {
  return (
    <div className="flex flex-col items-center px-10 gap-10">
      <Image
        src="/OCMSC_Portfolioslides.png"
        alt="dothecensus"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full -mt-8 -mb-14"
      />
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={`${styles.title}`}>
            <h1 className="max-[400px]:text-4xl">
              Ohlone College Multicultural
            </h1>
            <p className="max-[400px]:text-4xl">Student Center</p>
          </div>
          <p className={styles.tp}>
            Visual Identity // Branding // Logo Design
          </p>
        </div>
        <p className={styles.p}>
          The Multicultural Student Center (MSC) goal is to promote student
          success, retention, persistence, and degree completion by fostering
          holistic identity development that embraces diversity and prepares
          students to become conscious global citizens. As their contracted
          Visual Designer I was tasked to create a welcoming, inspiring, and
          culturally sensitive visual identity that could represent its diverse
          student population.
        </p>
      </div>
      <Image
        src="/OCMSC_Mockup.png"
        alt="markup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/OCMSC_Portfolioslides3.png"
        alt="markup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/OCMSC_Portfolioslides4.png"
        alt="markup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/OCMSC_Portfolioslides2.png"
        alt="markup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className="flex justify-between w-full pt-20 gap-10 max-mobile:gap-5">
        <Link
          href="/annual-reports"
          className="flex flex-row items-center gap-3"
        >
          <SlArrowLeft className="text-2xl " />
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            Annual Reports
          </p>
        </Link>
        <Link href="/frn" className="flex flex-row items-center">
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            FRN
          </p>
          <SlArrowRight className="text-2xl" />
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
