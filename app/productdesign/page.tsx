"use client";
import { designProjects } from "@/index";
import Image from "next/image";
import Link from "next/link";
import React, { useRef, useState } from "react";
import { motion, useScroll, useTransform } from "framer-motion";
import GetinTouch from "@/components/GetinTouch";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";

import { myprocess } from "@/index";

const Page = () => {
  const ref = useRef<HTMLDivElement>(null);
  const { scrollY } = useScroll({
    target: ref,
  });
  const opacityProgressGrid = useTransform(scrollY, [100, 300], [0, 1]);
  const opacityProgressDrop = useTransform(scrollY, [200, 400], [0, 1]);
  const [discoverActive, setDiscoverActive] = useState(false);
  const [defineActive, setDefineActive] = useState(false);
  const [ideateActive, setIdeateActive] = useState(false);
  const [testActive, setTestActive] = useState(false);
  const [implementActive, setImplementActive] = useState(false);
  const [validateActive, setValidateActive] = useState(false);

  type DropdownProps = {
    number: number;
  };
  // const Dropodown: React.FC<DropdownProps> = ({ number }) => {
  //   const process = myprocess[number];
  //   console.log(`this is the proces ${process}`);
  //   console.log(`this is the proces ${process.name}`);
  //   // Check if process is an array before mapping over it
  //   if (!Array.isArray(process)) {
  //     return null; // or some fallback component
  //   }

  //   return (
  //     <motion.ul
  //       initial={{ opacity: 0, y: -20 }}
  //       animate={{ opacity: 1, y: 0, transition: { duration: 0.9 } }}
  //       exit={{ opacity: 0, y: -20 }}
  //       className="flex justify-start items-start "
  //     >
  //       <div className="flex justify-end items-end">
  //         {process.map((item, index) => {
  //           console.log(item); // Moved console.log here
  //           return (
  //             <li key={index}>
  //               {(Object.values(item) as string[]).map((value, idx) => (  {/* cast to string[] */}

  //                 <div key={idx} >
  //                   {value}
  //                 </div>
  //               ))}
  //             </li>
  //           );
  //         })}
  //       </div>
  //     </motion.ul>
  //   );

  return (
    <div>
      {/* <Navbar /> */}
      <div className="flex flex-row justify-around gap-5 mx-11 my-16 max-mobile:flex-col max-mobile:items-center">
        <div className="w-64 h-64 max- rounded-full overflow-hidden">
          <Image
            src="/Miguel-product-design.jpg"
            alt="cover picture"
            // width={0}
            // height={0}
            // sizes="100vw"
            // className="w-full h-auto "
            width={8000}
            height={80000}
            quality={100}
            className="-translate-y-16"
          ></Image>
        </div>
        <div className="flex flex-col w-[70%] gap-3 max-mobile:w-full">
          <h1 className="capitalize text-6xl max-[1000px]:text-3xl font-bold pb-5 text-nowrap">
            product Designer
          </h1>
          <p className="max-mobile:pb-5">
            California-based design leader building innovative solutions for
            social justice initiatives. I have over 5 years of experience in the
            design industry and bring an enthusiastic and upbeat attitude to the
            workplace.
          </p>
          <div className="flex flex-row gap-10 max-mobile:flex-col">
            <div>
              <h1 className="font-bold">San Francisco State University</h1>
              <p>B.S Visual Communication Design</p>
            </div>
            <div>
              <h1 className="font-bold">
                Mission Asset Fund - FinTech Nonprofit
              </h1>
              <p>UX/UI Designer</p>
            </div>
            <div>
              <h1 className="font-bold">PowerMyLearning - EdTech Nonprofit</h1>
              <p>Product Designer</p>
            </div>
          </div>
        </div>
      </div>
      <div className="mx-11 mb-11">
        <h1 className="text-5xl mb-10">Design Projects</h1>
        <div className="flex flex-row justify-between max-mobile:flex-col max-mobile:justi">
          <div className="mr-4 max-md:pb-5">
            <h1 className="text-xl font-bold mb-5">Loan application design</h1>
            <p>
              I redesigned two critical parts of our loan application form to
              ensure they were user-friendly.
            </p>
            <p className="ml-7">
              1. Income & debt self-reporting section mobile redesign.
            </p>
            <p className="ml-7">2. Document upload section mobile redesign.</p>
          </div>
          <div>
            <h1 className="text-xl font-bold mb-5">MyMAF App design</h1>
            <p className="ml-7">
              1. I lead a UX research project to add a new engaging video
              feature for our Financial Recovery Content Module.{" "}
            </p>
          </div>
        </div>
      </div>
      <motion.div
        className="grid grid-cols-2 justify-items-center gap-5 px-5 max-mobile:grid-cols-1"
        ref={ref}
        style={{
          opacity: opacityProgressGrid,
          transition: "opacity 1.1s ease",
        }}
      >
        {designProjects.map((project) => (
          <div key={project.id} className="relative overflow-hidden w-full ">
            <Link href="/" key={project.id}>
              <Image
                src={project.image}
                alt="image"
                width={10}
                height={10}
                sizes="100vw"
                className="w-full h-auto"
              ></Image>
              {/* <div
                style={{ backgroundImage: `url(${project.image})` }}
                className="h-[594px] w-[692px] bg-cover bg-center hover:scale-110 duration-500 object-cover transition-transform transform"
              ></div> */}
            </Link>
            <div className="pt-8">
              <h1 className="absolute bottom-0 left-0 right-0 text-center bg-white opacity-100 py-2">
                {project.name}
              </h1>
            </div>
          </div>
        ))}
      </motion.div>
      <div className="flex px-5">
        <Accordion
          type="single"
          collapsible
          className="w-2/6 max-mobile:w-full text-base"
        >
          <AccordionItem value="item-1" className="">
            <AccordionTrigger className="text-base">Discover</AccordionTrigger>
            <AccordionContent className="px-5 text-base">
              &#x2022; User Research
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; Target Audience
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; Constraints
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value="item-2">
            <AccordionTrigger className="text-base">Define</AccordionTrigger>
            <AccordionContent className="px-5 text-base">
              &#x2022; The Problem
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; The Goal
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; Competitive Analysis
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value="item-3">
            <AccordionTrigger className="text-base">Ideate</AccordionTrigger>
            <AccordionContent className="px-5 text-base">
              &#x2022; Wireframes{" "}
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; Low-fi mockups{" "}
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; Rapid Prototyping
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value="item-4">
            <AccordionTrigger className="text-base">Test</AccordionTrigger>
            <AccordionContent className="px-5 text-base" text-base>
              &#x2022; User testing
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; A/B Testing
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; High Fidelity Prototype
            </AccordionContent>
          </AccordionItem>
          <AccordionItem value="item-5">
            <AccordionTrigger className="text-base">Implement</AccordionTrigger>
            <AccordionContent className="px-5 text-base">
              &#x2022; Final Prototype
            </AccordionContent>
            <AccordionContent className="px-5 text-base">
              &#x2022; Developer Handoff
            </AccordionContent>
            <AccordionItem value="item-6">
              <AccordionTrigger className="text-base">
                Validate
              </AccordionTrigger>
              <AccordionContent className="px-5 text-base">
                &#x2022; Product Analysis
              </AccordionContent>
              <AccordionContent className="px-5 text-base">
                &#x2022; User Feedback
              </AccordionContent>
              <AccordionContent className="px-5 text-base">
                &#x2022; Usability Testing
              </AccordionContent>
              <AccordionContent className="px-5 text-base">
                &#x2022; QA Testing
              </AccordionContent>
              <AccordionContent className="px-5 text-base">
                &#x2022; Retrospective
              </AccordionContent>
            </AccordionItem>
          </AccordionItem>
        </Accordion>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;

// <motion.div
//   className="flex flex-col mt-48 mx-16"
//   ref={ref}
//   style={{
//     opacity: opacityProgressDrop,
//     transition: "opacity 1.1s ease",
//   }}
// >
//   <p className="w-[400px] border-b-2 ">My Process</p>
//   <div className="w-[400px] min-h-[60px] border-b-2 flex flex-col  items-center">
//     <button
//       className="flex justify-between w-full h-full items-center pt-4"
//       onClick={() => {
//         console.log("Button clicked");
//         setDiscoverActive(!discoverActive);
//       }}
//     >
//       <p>Discover</p>
//       <Image
//         alt="cheveron"
//         height={10}
//         width={15}
//         src="/down cheveron.png"
//       ></Image>
//     </button>
//     {/* {isActive && <Dropodown number={1} />} */}
//     {discoverActive && (
//       <div className="">
//         <ul>
//           <li>User Research</li>
//           <li>Target Audience</li>
//           <li>Constraints</li>
//         </ul>
//       </div>
//     )}
//   </div>
//   <div className="w-[400px] min-h-[60px] border-b-2 flex flex-col items-center">
//     <button
//       className="flex justify-between w-full h-full items-center pt-4"
//       onClick={() => {
//         console.log("Button clicked");
//         setDefineActive(!defineActive);
//       }}
//     >
//       <p>Define</p>
//       <Image
//         alt="cheveron"
//         height={10}
//         width={15}
//         src="/down cheveron.png"
//       ></Image>
//     </button>
//     {defineActive && (
//       <div className="">
//         <ul>
//           <li>The problem</li>
//           <li>The goal</li>
//           <li>Competitive Analysis</li>
//         </ul>
//       </div>
//     )}
//   </div>
//   <div className="w-[400px] min-h-[60px] border-b-2 flex flex-col items-center">
//     <button
//       className="flex justify-between w-full h-full items-center pt-4"
//       onClick={() => {
//         console.log("Button clicked");
//         setIdeateActive(!ideateActive);
//       }}
//     >
//       <p>Ideate</p>
//       <Image
//         alt="cheveron"
//         height={10}
//         width={15}
//         src="/down cheveron.png"
//       ></Image>
//     </button>
//     {ideateActive && (
//       <div className="">
//         <ul>
//           <li>Wireframes</li>
//           <li>Low-fi mockups</li>
//           <li>Rapid Prototyping</li>
//         </ul>
//       </div>
//     )}
//   </div>
//   <div className="w-[400px] min-h-[60px] border-b-2 flex flex-col items-center">
//     <button
//       className="flex justify-between w-full h-full items-center pt-4"
//       onClick={() => {
//         console.log("Button clicked");
//         setTestActive(!testActive);
//       }}
//     >
//       <p>Test</p>
//       <Image
//         alt="cheveron"
//         height={10}
//         width={15}
//         src="/down cheveron.png"
//       ></Image>
//     </button>
//     {testActive && (
//       <div className="">
//         <ul>
//           <li>User testing</li>
//           <li>A/B Testing</li>
//           <li>High Fidelity Prototype</li>
//         </ul>
//       </div>
//     )}
//   </div>
//   <div className="w-[400px] min-h-[60px] border-b-2 flex flex-col items-center">
//     <button
//       className="flex justify-between w-full h-full items-center pt-4"
//       onClick={() => {
//         console.log("Button clicked");
//         setImplementActive(!implementActive);
//       }}
//     >
//       <p>Implement</p>
//       <Image
//         alt="cheveron"
//         height={10}
//         width={15}
//         src="/down cheveron.png"
//       ></Image>
//     </button>
//     {implementActive && (
//       <div className="">
//         <ul>
//           <li>Final Prototype </li>
//           <li>Developer Handoff</li>
//           {/* <li>Competitive Analysis</li> */}
//         </ul>
//       </div>
//     )}
//   </div>
//   <div className="w-[400px] min-h-[60px] border-b-2 flex flex-col items-center">
//     <button
//       className="flex justify-between w-full h-full items-center pt-4"
//       onClick={() => {
//         console.log("Button clicked");
//         setValidateActive(!validateActive);
//       }}
//     >
//       <p>Validate</p>
//       <Image
//         alt="cheveron"
//         height={10}
//         width={15}
//         src="/down cheveron.png"
//       ></Image>
//     </button>
//     {validateActive && (
//       <div className="">
//         <ul>
//           <li>Developer Handoff</li>
//           <li>User Feedback </li>
//           <li>Usability Testing</li>
//           <li>QA Testing</li>
//           <li>Retrospective</li>
//         </ul>
//       </div>
//     )}
//   </div>
// </motion.div>
