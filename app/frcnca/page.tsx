import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";
import styles from "../style.module.scss";
import Link from "next/link";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";

const Page = () => {
  return (
    <div className="flex flex-col items-center px-10 gap-10">
      <Image
        src="/FRCNCA_Cover.png"
        alt="FRCNCA_coveer"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full -mt-10 -mb-20"
      />
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={styles.title}>
            <h1>Family Resource Centers</h1>
            <p>Network of California</p>
          </div>
          <p className={styles.tp}>
            Visual Identity // Branding // Logo Design
          </p>
        </div>
        <p className={styles.p}>
          Family Resource Centers Network of California (FRCNCA) is a coalition
          of 45 Early Start FRCs in California. The network provides access to
          cutting-edge professional development and enhanced connections with
          other professionals in the field. As their contracted Visual Designer
          I was tasked to create a modern, playful, and unifying visual identity
          that could represent all 45+ FRC’s in the network.
        </p>
      </div>
      <Image
        src="/Mockup.png"
        alt="handdrawing"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/FRCNCA_Logos.png"
        alt="FRCNCA logos"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/FRCNCA_StyleGuide.png"
        alt="FRCNCA_StyleGuide"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/FRCNCA_MockUP2.png"
        alt="FRCNCA_MockUp2.png"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className="flex justify-between w-full pt-20 gap-10 max-mobile:gap-5">
        <Link href="/frn" className="flex flex-row items-center gap-3">
          <SlArrowLeft className="text-2xl " />
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            FRN
          </p>
        </Link>
        <Link
          href="/salon-nuna"
          className="flex flex-row items-center max-[311px]:w-min"
        >
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            Salon Nuna
          </p>
          <SlArrowRight className="text-2xl" />
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
