import type { Metadata } from "next";
// import { Inter } from 'next/font/google'
import "./globals.css";
import Header from "../components/Header";
import Navbar from "@/components/Navbar";
import ScrollToTop from "@/components/ScrollToTop";

// const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  metadataBase: new URL("https://www.miguelcastillodesign.com"), // Set the base URL
  title: "Miguel's Portfilio",
  description: "Miguels Castillo Design Portfolio",
  openGraph: {
    title: "Miguel's Portfolio",
    description: "A showcase of Miguel Castillo's design work and projects.",
    url: "https://www.miguelcastillodesign.com", // Replace with your actual URL
    siteName: "Miguel's Portfolio",
    images: [
      {
        url: "/favicon.ico", // Replace with your thumbnail URL
        width: 1200,
        height: 630,
        alt: "Miguel Castillo's Design Portfolio Thumbnail",
      },
    ],
    locale: "en_US",
    type: "website",
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <Navbar />
        <ScrollToTop />
        <main>{children}</main>
      </body>
    </html>
  );
}
