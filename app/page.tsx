import Navbar from "@/components/Navbar";
import Grids from "@/sections/Grids";

export default function Home() {
  return (
    <>
      {/* <Navbar/> */}
      <Grids />
    </>
    // <main>
    //   {/* <Navbar/> */}
    //   <Grids />
    // </main>
  );
}
