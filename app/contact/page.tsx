import React from "react";
import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";

const Page = () => {
  return (
    <div>
      <div className="flex flex-row justify-around gap-5 mx-11 my-16 max-mobile:flex-col max-mobile:items-center">
        <div className="w-64 h-64 max- rounded-full overflow-hidden">
          <Image
            src="/headshot_miguel_edit.png"
            alt="cover picture"
            width={0}
            height={0}
            sizes="100vw"
            className="w-full h-auto "
            // width={8000}
            // height={80000}
            // quality={100}
            // className="-translate-y-16"
          ></Image>
        </div>
        <div className="flex flex-col w-[70%] gap-3 max-mobile:w-full">
          <h1 className="capitalize text-6xl max-[1000px]:text-3xl font-bold pb-5 text-nowrap">
            Visual Designer
          </h1>
          <p className="max-mobile:pb-5">
            California-based design leader building innovative solutions for
            social justice initiatives. I have over 8 years of experience in the
            design industry and bring an enthusiastic and upbeat attitude to the
            workplace.
          </p>
          <div className="flex flex-row gap-10 max-mobile:flex-col max-[848px]:flex-wrap">
            <div>
              <h1 className="font-bold">
                Kooth.US, Soluna App - Digital Health
              </h1>
              <p>Senior Product Designer</p>
            </div>
            <div>
              <h1 className="font-bold">
                Mission Asset Fund - FinTech Nonprofit
              </h1>
              <p>UX/UI Designer</p>
            </div>
            <div>
              <h1 className="font-bold">PowerMyLearning - EdTech Nonprofit</h1>
              <p>Product Designer</p>
            </div>
            <div>
              <h1 className="font-bold">San Francisco State University</h1>
              <p>B.S Visual Communication Design</p>
            </div>
          </div>
        </div>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;

// <div className="flex max-[901px]:flex-col justify-center gap-4 px-10 pt-10 ">
//   {/* <div className="flex grid-cols-3 justify-center max-[990px]:flex-col gap-5 overflow-hidden"> */}
//   {/* <div className="w-4/6 [901px]:w-4/6 rounded-3xl relative h-full max-w-full [901px]:max-w-none [901px]:h-auto mx-auto"> */}
//   <div className="w-4/6 rounded-3xl relative max-[901px]:w-full max-[901px]:h-52">
//     <Image
//       alt="cover picture"
//       src="/Miguel-contact.png"
//       // width={0}
//       // height={0}
//       // sizes="100vw"
//       // className="h-full w-auto max-[990px]:w-auto max-[990px]:h-[400px]"
//       // height={1}
//       // width={1}
//       // sizes="100vw"
//       fill
//       className=" rounded-3xl object-cover object-center "
//     ></Image>
//   </div>
//   <div className="flex w-4/6 gap-4 max-[901px]:flex-col-reverse max-[901px]:w-full max-[901px]:gap-20">
//     <div className="flex flex-col w-1/2 max-[901px]:w-full max-[901px]:h-2/6">
//       <h1 className="mb-10 font-bold">Hi, I&apos;m Miguel!</h1>
//       <p className="mb-8">
//         I&apos;m a Visual Designer based in California.{" "}
//       </p>
//       <p className="mb-8">
//         Upon pursuing my creative passions, I&aposve always been driven to
//         empower the voices of our communities through dynamic visual media
//         and meaningful experiences.
//       </p>
//       <p className="mb-5">
//         I approach each project with empathy and a genuine desire to make
//         a positive impact. My background as an immigrant, gay, and person
//         of color has fueled my commitment to supporting nonprofits in
//         their efforts to serve their targeted communities to create a more
//         equitable and just society. I thrive on the opportunity to use my
//         design skills to amplify the voices of marginalized communities
//         and advocate for social change.
//       </p>
//       <div className="flex justify-start">
//         <a
//           href=""
//           className="bg-black rounded-full py-5 px-16 w-min text-white flex flex-col justify-center items-center"
//         >
//           <p className="text-nowrap">Lets Work</p>
//           <p>Together</p>
//         </a>
//       </div>
//     </div>
//     <div className="flex flex-col w-1/2 max-[901px]:w-full max-[901px]:h-2/6 ">
//       <h1 className="mb-10 font-bold">Skills</h1>
//       <p className="mb-8">
//         <span className="font-bold">Graphic Design:</span> Typography,
//         Composition, Color Theory, Branding+ Identity, Print + Digital
//         design, Motion graphics, Video production, Social media, and
//         Design thinking.
//       </p>
//       <p>
//         <span className="font-bold">Product Design:</span> UX strategy,
//         User flows, Concept sketches, Journey maps, Wireframes,
//         Prototypes, Mockups, Motion design, Design systems, and Branding.
//       </p>
//       <p>
//         <span className="font-bold">Research:</span> User interviews,
//         Usability testing, Persona hypothesis, Competitive analysis, and
//         A/B testing.
//       </p>
//       <p className="mb-10">
//         <span className="font-bold">Software:</span> Figma, Sketch, JIRA,
//         Confluence, Mural, Asana, Photoshop, Illustrator, InDesign, After
//         Effects, Premiere Pro, and Canva.{" "}
//       </p>
//       <p className="font-bold">Email</p>
//       <p>Miguelcas.design@gmail.com</p>
//     </div>
//   </div>
// </div>
