import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";
import styles from "../style.module.scss";
import { Button, buttonVariants } from "@/components/ui/button";
import Link from "next/link";
import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui/carousel";
import { carousel } from "@/index";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";

const Page = () => {
  return (
    <div className="px-10">
      <div className="mx-auto ">
        <Carousel className="w-full " opts={{ loop: true }}>
          <CarouselContent className="-ml-10">
            {carousel.map((pics) => (
              <CarouselItem
                key={pics.id}
                className="-pl-1 md:basis-1/2 lg:basis-1/2"
              >
                <Image
                  src={pics.src}
                  alt="image"
                  width={0}
                  height={0}
                  sizes="100vw"
                  className="w-auto"
                />
              </CarouselItem>
            ))}
          </CarouselContent>
          <CarouselPrevious />
          <CarouselNext />
        </Carousel>
      </div>
      {/* <Image
        src="/ARCover1.png"
        alt="arcover1"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      /> */}
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={styles.title}>
            <h1>Annual Reports</h1>
          </div>
          <p className={styles.tp}>Print + Digital Design // Infographics</p>
        </div>
        <p className={styles.p}>
          I design Annual Reports for nonprofits, small businesses, and
          foundations. They include infographics, financial tables, and charts,
          with a clean modern layout. The text and copy are provided through a
          Google doc and handed over to me for the design. The report goes
          through several design reviews in order to receive constructive
          feedback to reach our final design.
        </p>
      </div>

      <h1 className="text-3xl font-bold py-10 text-left w-full">
        Council of Parent Attorneys and Advocates, COPAA
      </h1>
      {/* <div className="relative w-full h-[600px] max-tablet:h-[450px] max-mobile:h-[300px] xlscreen:h-[800px] mb-3"> */}
      <Image
        src="/COPAA_1.png"
        alt="COPAA"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full pb-5"
      />
      {/* </div> */}
      <Image
        src="/COPPA_2.png"
        alt="COPAA2"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />

      <div className="flex justify-center pt-10">
        <Button asChild>
          <Link
            href="/COPAA_AnnualReport_2022.pdf"
            target="_blank"
            rel="noopener"
            className="max-mobile:h-9 max-mobile:px-4 max-mobile:py-2 max-mobile:text-sm"
          >
            COPAA Annual Report 2022
          </Link>
        </Button>
      </div>

      {/* <div className="flex justify-center pt-10">
        <a
          href=""
          className="flex flex-col items-center bg-black text-white rounded-full p-7 max-mobile:p-5 max-mobile:text-sm text-xl hover:bg-stone-800 transition-all duration-300"
        >
          <p>COPPA Annual</p>
          <p>Report 2022</p>
        </a>
      </div> */}
      <h1 className="text-3xl font-bold py-10 text-left w-full">
        Good Samaritan Family Resource Center
      </h1>
      <div className="flex flex-col gap-5">
        <Image
          src="/GoodSamAR1.png"
          alt="goodsamar1"
          width={0}
          height={0}
          sizes="100vw"
          className="w-full"
        />
        <Image
          src="/GoodSamAR2.png"
          alt="goodsamar2"
          width={0}
          height={0}
          sizes="100vw"
          className="w-full"
        />
        <Image
          src="/GoodSamAR3.png"
          alt="goodsamar3"
          width={0}
          height={0}
          sizes="100vw"
          className="w-full"
        />
      </div>
      <div className="flex justify-center items-center max-mobile:flex-col max-mobile:gap-3 pt-10 gap-[3%]">
        {/* <a
          href=""
          className="bg-black text-white text-center rounded-full p-7 max-mobile:p-5 max-mobile:text-sm hover:bg-stone-800 transition-all duration-300"
        >
          <p>Good Samaritan</p>
          <p>Annual Report</p>
          <p>2021</p>
        </a> */}
        <Button asChild>
          <Link
            href="/GoodSam_AnnualReport_Final_Singles.pdf"
            target="_blank"
            rel="noopener"
            className="max-mobile:h-9 max-mobile:px-4 max-mobile:py-2 max-mobile:text-sm"
          >
            Good Samaritan Annual Report 2021
          </Link>
        </Button>
        <Button asChild>
          <Link
            href="/GoodSam_AnnualReport_2022.pdf"
            target="_blank"
            rel="noopener"
            className="max-mobile:h-9 max-mobile:px-4 max-mobile:py-2 max-mobile:text-sm"
          >
            Good Samaritan Annual Report 2022
          </Link>
        </Button>
        {/* <a
          href=""
          className="bg-black text-white text-center rounded-full p-7 max-mobile:p-5 max-mobile:text-sm hover:bg-stone-800 transition-all duration-300"
          >
          <p>Good Samaritan</p>
          <p>Annual Report</p>
          <p>2022</p>
          </a> */}
      </div>
      <div className="flex justify-between pt-20 gap-10 max-mobile:gap-5 max-[355px]:ml-[-20px]">
        <Link href="/bayview" className="flex flex-row items-center gap-3">
          <SlArrowLeft className="text-2xl " />
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            Bayview Hunters Point Children&apos;s Oral Health Taskforce
          </p>
        </Link>
        <Link href="/ohlone-collage" className="flex flex-row items-center">
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            Ohlone College Multicultural Student Center
          </p>
          <SlArrowRight className="text-2xl" />
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
