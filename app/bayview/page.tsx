import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";
import styles from "../style.module.scss";
import Link from "next/link";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";

const Page = () => {
  return (
    <div className="flex flex-col items-center px-10 gap-10">
      <Image
        src="/D10_Portfolioslides.png"
        alt="dothecensus"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full -mt-20 -mb-24"
      />
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={styles.title}>
            <h1>Bayview Hunters Point </h1>
            <p>Children&apos;s Oral Health</p>
            <p>Taskforce</p>
          </div>
          <p className={styles.tp}>
            Visual Identity // Branding // Logo Design // Print Design
          </p>
        </div>
        <p className={styles.p}>
          The District 10 Children&apos;s Oral Health Taskforce, hosted by APA
          Family Support Services, are working to promote safe and effective
          oral health information and access with a special emphasis on
          African-American children throughout the District 10 area (Bayview and
          Hunters Point). These illustrations were hand drawn by Nancy Cato and
          digitized by me to produce a new logo, color palette, and print and
          digital materials.
        </p>
      </div>
      <Image
        src="/D10-Mockup!.png"
        alt="bayfiew marketing"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/D10_Portfolioslides3.png"
        alt="bayfiew 3slides"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/D10_Portfolioslides4.png"
        alt="bayfiew 3slides"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/D10_Portfolioslides2.png"
        alt="bayfiew collors"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/D10-Mockup2.png"
        alt="bayfiew tri marketing"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <h1 className="text-3xl py-10">Download Brochures!</h1>
      <div className="flex max-[875px]:flex-col gap-3 max-[875px]:gap-4">
        <a
          href="/Ages&Stages_BVHP_COHT_Brochures.pdf"
          target="_blank"
          rel="noopener"
          className="bg-black text-white text-center rounded-full px-8 py-8 hover:bg-stone-800 transition-all duration-300"
        >
          Ages and Stages of Optimal Oral Health
        </a>
        <a
          href="/Children&Teens_BVHP_COHT_Brochures.pdf"
          target="_blank"
          rel="noopener"
          className="bg-black text-white text-center rounded-full px-8 py-8 hover:bg-stone-800 transition-all duration-300"
        >
          Oral Health Guidelines for Children and Teens
        </a>
        <a
          href="/Family&Community_BVHP_COHT_Brochures.pdf"
          target="_blank"
          rel="noopener"
          className="bg-black text-white text-center rounded-full px-8 py-8 hover:bg-stone-800 transition-all duration-300"
        >
          Standing with your Family Standing with your Community
        </a>
        <a
          href="/Newborns&0-5Year_BVHP_COHT_Brochures.pdf"
          target="_blank"
          rel="noopener"
          className="bg-black text-white text-center rounded-full px-8 py-8 hover:bg-stone-800 transition-all duration-300"
        >
          Oral Health Guidelines for Newborns and 0-5 Year Olds
        </a>
        <a
          href="/Parents&Providers_BVHP_COHT_Brochures.pdf"
          target="_blank"
          rel="noopener"
          className="bg-black text-white text-center rounded-full px-8 py-8 hover:bg-stone-800 transition-all duration-300"
        >
          Oral Health Guidelines for Parents and Providers
        </a>
        <a
          href="/SpecialNeeds_BVHP_COHT_Brochures.pdf"
          target="_blank"
          rel="noopener"
          className="bg-black text-white text-center rounded-full px-8 py-8 hover:bg-stone-800 transition-all duration-300"
        >
          Oral Health, Dental Care And Special Needs
        </a>
      </div>
      <div className="flex justify-between w-full pt-20 gap-10 max-mobile:gap-5">
        <Link href="/sfcounts" className="flex flex-row items-center gap-3">
          {/* <SlArrowLeft className="text-2xl " /> */}
          <Image alt="arrow" src="/ArrowLeft.png" width={25} height={25} />
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            SF Counts
          </p>
        </Link>
        <Link
          href="/annual-reports"
          className="flex flex-row items-center max-[435px]:w-min"
        >
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            Annual Reports
          </p>
          {/* <SlArrowRight className="text-2xl" /> */}
          <Image alt="arrow" src="/ArrowRight.png" width={25} height={25} />
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
