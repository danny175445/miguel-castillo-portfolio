import React from "react";
import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";
import styles from "../style.module.scss";
import Link from "next/link";
import { SlArrowLeft, SlArrowRight } from "react-icons/sl";
const Page = () => {
  return (
    <div className="flex flex-col items-center px-10 gap-10">
      <Image
        src="/SalonNuna_Cover.png"
        alt="SalonNuna_Cover"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={styles.title}>
            <h1>Salon Nuna</h1>
          </div>
          <p className={styles.tp}>
            Visual Identity // Branding // Logo Design
          </p>
        </div>
        <p className={styles.p}>
          Salon Nuna is a San Francisco-based hair salon dedicated to
          celebrating each individual&apos;s unique style and personality. As
          the Visual Designer, my role was to craft a refined, minimalist, and
          contemporary visual identity.
        </p>
      </div>
      <Image
        src="/MainMockUp_SolonNuna.png"
        alt="Main Mockup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Salon_Nuna_Logos.png"
        alt="salon nuna logogs"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/Salon_Nuna_Logos2.png"
        alt="salon nuna logogs2"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/SalonNuna_StyleGuide.png"
        alt="salon nuna styleGuide"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <Image
        src="/NUNA7.png"
        alt="nuna7"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full"
      />
      <div className="flex justify-between w-full pt-20 gap-10 max-mobile:gap-5 max-[355px]:ml-[-20px]">
        <Link href="/frcnca" className="flex flex-row items-center gap-3">
          <SlArrowLeft className="text-2xl " />
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            FRCNCA
          </p>
        </Link>
        <Link
          href="/monday-or-tuesday"
          className="flex flex-row items-center max-[435px]:w-min"
        >
          <p className="text-4xl font-bold max-tablet:text-3xl max-mobile:text-xl">
            <span className="text-nowrap">Monday Or</span> Tuesday
          </p>
          <SlArrowRight className="text-2xl " />
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
