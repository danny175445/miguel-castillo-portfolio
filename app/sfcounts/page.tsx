import GetinTouch from "@/components/GetinTouch";
import Image from "next/image";
import React from "react";
import styles from "../style.module.scss";
import { Button } from "@/components/ui/button";
import Link from "next/link";
import { SlArrowRight } from "react-icons/sl";

const Page = () => {
  return (
    <div className="flex flex-col items-center px-10 gap-10">
      <Image
        src="/DotheCensus_BigCover.png"
        alt="dothecensus"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <div className={styles.text}>
        <div className={styles.name}>
          <div className={styles.title}>
            <h1>SF COUNTS</h1>
            <p>2020 Census</p>
          </div>
          <p className={styles.tp}>Campaign // Print + Digital Design </p>
        </div>
        <p className={styles.p}>
          SF Counts 2020 is a city-wide campaign organized by the city of San
          Francisco with the purpose of igniting San Franciscans to complete the
          2020 Census despite stereotypes and barriers they may face. The
          campaign includes information in 15 languages. As the Visual Designer
          behind the campaign, I was tasked to create a simple, effective, and
          inclusive design that would inform and activate San Francisco
          residents to complete the census. The campaign includes Posters,
          Postcards, and Social Media Graphics.
        </p>
      </div>
      <Image
        src="/DotheCensus_MultiLanguages.png"
        alt="dothecensus"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <Image
        src="/Census2020_Sept24_26.JPG"
        alt="cencus2020"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />

      <Image
        src="/DotheCensus_MAINHEADERS.png"
        alt="dothecensus"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <div className="relative w-full h-auto overflow-hidden translate-x-[0%]">
        <Image
          src="/Dothecensus_PosterMockUp_Blue.png"
          alt="dothecensus"
          width={0}
          height={0}
          sizes="100vw"
          className="w-full h-auto object-cover"
          style={{
            objectPosition: "center",
            objectFit: "cover",
            // width: "100%",
            // height: "auto",
            clipPath: "inset(0% 13% 13% 0%)", // Adjust percentages as needed to crop the edges
            transform: "translatex(10%) translatey(10%) scale(1.2)",
          }}
        />
      </div>
      <h1 className="text-3xl py-10 text-left w-full">Posters</h1>
      <Image
        src="/SFCOUNTS_Posters.png"
        alt="sfcounts grids"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <Image
        src="/DSC06418.JPG"
        alt="speach"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />

      <h1 className="text-3xl py-10 text-left w-full">Post Cards</h1>

      <Image
        src="/Census2020_Deliverbles2.png"
        alt="deliverbles2"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />

      <Image
        src="/Census2020_Deliverbles.png"
        alt="deliverbles"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <h1 className="text-3xl py-10 text-left w-full">Bus Banners</h1>
      <div className="w-full flex flex-col gap-5 ">
        <Image
          src="/2020Census_CravanBanner_Final.png"
          alt="Banner"
          width={0}
          height={0}
          sizes="100vw"
          className="w-full h-auto"
        />
        <Image
          src="/2020Census_CravanBanner_Final2.png"
          alt="Banner"
          width={0}
          height={0}
          sizes="100vw"
          className="w-full h-auto"
        />
      </div>
      <div className="w-full flex max-mobile:flex-col">
        <Image
          src="/DSC06815.JPG"
          alt="bus"
          width={0}
          height={0}
          sizes="100vw"
          className="w-1/2 h-auto max-mobile:w-full max-mobile:pb-5 pr-5"
        />
        <Image
          src="/IMG_9309.jpeg"
          alt="bus"
          width={0}
          height={0}
          sizes="100vw"
          className="w-1/2 h-auto max-mobile:w-full"
        />
      </div>
      <Image
        src="/DotheCensus_StyleGuide.png"
        alt="Primary colors"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <h1 className="text-3xl py-10 text-left w-full">Motion Graphics</h1>
      <div className="flex w-full gap-3">
        <div className="relative w-full pb-[56.25%]">
          <iframe
            src="https://player.vimeo.com/video/494910430?badge=0&autopause=0&player_id=0&app_id=58479"
            className="absolute top-0 left-0 w-full h-full"
            allowFullScreen
            title="SFCounts 2020 - 10 Minutes"
          ></iframe>
        </div>
        <div className="relative w-full pb-[56.25%]">
          <iframe
            src="https://player.vimeo.com/video/494900600?badge=0&autopause=0&player_id=0&app_id=58479"
            className="absolute top-0 left-0 w-full h-full"
            allowFullScreen
            title="SFCounts 2020 - 10 Minutes"
          ></iframe>
        </div>
      </div>

      <div className="flex w-full gap-3">
        <div className="relative w-full pb-[56.25%]">
          <iframe
            src="https://player.vimeo.com/video/494911784?badge=0&autopause=0&player_id=0&app_id=58479"
            className="absolute top-0 left-0 w-full h-full"
            allowFullScreen
            title="SFCounts 2020 - 10 Minutes"
          ></iframe>
        </div>
        <div className="relative w-full pb-[56.25%]">
          <iframe
            src="https://player.vimeo.com/video/494914212?badge=0&autopause=0&player_id=0&app_id=58479"
            className="absolute top-0 left-0 w-full h-full"
            allowFullScreen
            title="SFCounts 2020 - 10 Minutes"
          ></iframe>
        </div>
      </div>

      <Button asChild>
        <a
          href="https://vimeo.com/user130033208"
          target="_blank"
          rel="nooperner"
          className="max-mobile:h-9 max-mobile:px-4 max-mobile:py-2 max-mobile:text-sm"
        >
          View more videos
        </a>
      </Button>
      {/* <a
        href="https://vimeo.com/user130033208"
        target="_blank"
        rel="noopener"
        className="bg-black text-white text-center rounded-full px-8 py-8 hover:bg-stone-800 transition-all duration-300"
      >
        View more videos
        {/* <p className="text-center font-bold text-xl">View more videos</p> */}
      {/* </a> */}
      <h1 className="text-3xl py-10 text-left w-full">Social Media</h1>
      <Image
        src="/SFCOUNTS_SMMockUPs4.png"
        alt="smockup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <Image
        src="/SFCOUNTS_SMMockUPs.png"
        alt="smockup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <Image
        src="/SFCOUNTS_SMMockUPs2.png"
        alt="smockup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <Image
        src="/SFCOUNTS_SMMockUPs3.png"
        alt="smockup"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      <div className="w-full flex max-mobile:flex-col">
        <Image
          src="/2020 Census Caravan.png"
          alt="carvan"
          width={0}
          height={0}
          sizes="100vw"
          className="w-1/2 h-auto max-mobile:w-full max-mobile:pb-5 pr-5"
        />
        <Image
          src="/DSC02346.JPG"
          alt="Carvan"
          width={0}
          height={0}
          sizes="100vw"
          className="w-1/2 h-auto max-mobile:w-full "
        />
      </div>
      <div className="w-full flex max-mobile:flex-col">
        <Image
          src="/DSC06799.JPG"
          alt="Imge"
          width={0}
          height={0}
          sizes="100vw"
          className="w-1/2 h-auto max-mobile:w-full max-mobile:pb-5 pr-5"
        />

        <Image
          src="/DSC06569.JPG"
          alt="Booth"
          width={0}
          height={0}
          sizes="100vw"
          className="w-1/2 h-auto max-mobile:w-full "
        />
      </div>
      <div className="flex w-full pt-20 justify-end max-mobile:justify-center">
        <Link
          href="/bayview"
          className="flex flex-row items-center w-1/2 gap-5"
        >
          <p className="text-4xl font-bold max-mobile:text-3xl text-right">
            Bayview Hunters Point Children&apos;s Oral Health Taskforce
          </p>
          {/* <SlArrowRight className="text-2xl" /> */}
          <Image alt="arrow" src="/ArrowRight.png" width={35} height={35} />
        </Link>
      </div>
      <GetinTouch />
    </div>
  );
};

export default Page;
