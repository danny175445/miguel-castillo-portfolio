import React from "react";
import { clients } from "..";
import Link from "next/link";
import Image from "next/image";
import GetinTouch from "@/components/GetinTouch";

const Grids = () => {
  return (
    <div className="flex flex-col gap-16">
      <div className="grid grid-cols-2 justify-items-center gap-2  px-10 max-singleGrid:grid-cols-1">
        {clients.map((client) => (
          // <Link href="" key={client.id}>
          <div key={client.id} className="w-full  relative group">
            <div className="w-full aspect-[4/3] relative overflow-hidden">
              <Link href={client.href}>
                <Image
                  alt="client Image"
                  src={client.image}
                  // fill
                  width={0}
                  height={0}
                  sizes="100vw"
                  className="w-full h-full object-cover"
                />
                <div className=" absolute inset-0 flex justify-center items-center text-center z-30 group-hover:bg-black/50 transition-opacity duration-300">
                  <h3
                    className={`text-4xl text-transparent group-hover:text-white hover:opacity-100 flex justify-center items-center text-center z-30 max-singleGrid:hidden`}
                  >
                    {client.name}
                  </h3>
                </div>
              </Link>
            </div>
            {/* <div className="w-[700px] h-[530px]  inset-0 bg-white opacity-0 group-hover:opacity-50 z-20"></div> */}
          </div>
          // </Link>
        ))}
      </div>
      <h1 className="text-5xl text-center">Clients</h1>
      {/* <div className="relative w-full aspect-auto"> */}
      <Image
        src="/Past_Clients_2023.png"
        alt="clients"
        width={0}
        height={0}
        sizes="100vw"
        className="w-full h-auto"
      />
      {/* </div> */}
      <GetinTouch />
    </div>
  );

  return (
    <div className="grid grid-cols-2 justify-items-center gap-5 mt-28">
      {clients.map((client) => (
        <Link
          href="/"
          key={client.id}
          className="relative group overflow-hidden h-[582px] w-[776px] bg-cover bg-center  z-10"
        >
          <div
            style={{ backgroundImage: `url(${client.image})` }}
            className="absolute inset-0 bg-cover bg-center"
          />
          <div className="relative flex justify-center items-center text-center z-30 w-full h-full">
            <h3 className="w-full h-full text-4xl text-transparent hover:text-black hover:opacity-100 flex justify-center items-center text-center z-30">
              {client.name}
            </h3>
          </div>
          <div className="absolute inset-0 bg-white opacity-0 group-hover:opacity-50 z-20"></div>
        </Link>
      ))}
    </div>
  );
};

export default Grids;
