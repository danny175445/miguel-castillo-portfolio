import index from "./components/Header";

export const clients = [
  {
    id: 1,
    name: "SF Counts",
    image: "/DotheCensus_PosterSplash_Blue.png",
    href: "/sfcounts",
  },
  {
    id: 2,
    name: "Bayview Hunters Point Children's Oral Health Taskforce",
    image: "/BVHPTaskforce_Mockup.png",
    href: "/bayview",
  },
  {
    id: 3,
    name: "Annual Reports",
    image: "/COPAA_Mockup1.png",
    href: "/annual-reports",
  },
  {
    id: 4,
    name: "Ohlone College Multicultural Student Center",
    image: "/Ractangle Cards PSD Mockup.png",
    href: "/ohlone-collage",
  },
  { id: 5, name: "FRN", image: "/FRNCardsMock.png", href: "/frn" },
  { id: 6, name: "FRCNCA", image: "/FRCNCA_CoverImage.png", href: "/frcnca" },
  {
    id: 7,
    name: "Salon Nuna",
    image: "/SalonNuna_Cover2.png",
    href: "/salon-nuna",
  },
  {
    id: 8,
    name: "Monday or Tuesday",
    image: "/MoT_MockUp.png",
    href: "/monday-or-tuesday",
  },
];
export const designProjects = [
  {
    id: 1,
    name: "Income & debt self reporting section redesign",
    image: "/MonthlyIncome_Debt_Mockup.png",
  },
  {
    id: 2,
    name: "Document upload section redesign",
    image: "/POI_Mockup1.png",
  },
  {
    id: 3,
    name: "Financial Recovery Content Module - MyMAF",
    image: "/RFM_Mockup.png",
  },
];

export const myprocess = [
  {
    name: "discover",
    text1: "User Reasearch",
    text2: "Target Audience",
    text3: "Constraints",
  },
  {
    name: "define",
    text1: "The Problem",
    text2: "The Goal",
    text3: "Coompetitive Analysis",
  },
  {
    name: "idete",
    text1: "Wireframes",
    text2: "Low-fi mockups",
    text3: "Rapid Prototyping",
  },
  {
    name: "test",
    text1: "User Testing",
    text2: "A/B Testing",
    text3: "High Fidelity Prototype",
  },
  {
    name: "impliment",
    text1: "Final Prototype",
    text2: "Developer Handoff",
  },
  {
    name: "validate",
    text1: "Prouct Analysis",
    text2: "User Feedback",
    text3: "usablity Testing",
    text4: "QA Testing",
    text5: "Retrospective",
  },
];

export const carousel = [
  {
    id: 1,
    src: "/ARCover1.png",
  },
  {
    id: 2,
    src: "/ARCover3.png",
  },
  {
    id: 3,
    src: "/ARCover2.png",
  },
  {
    id: 4,
    src: "/ARCover1.png",
  },
  {
    id: 5,
    src: "/ARCover3.png",
  },
  {
    id: 6,
    src: "/ARCover2.png",
  },
];
// /image5.png
