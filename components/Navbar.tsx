"use client";
import React, { useState, useEffect } from "react";
import Image from "next/image";
import "hamburgers/dist/hamburgers.css";
import { motion, AnimatePresence } from "framer-motion";
import GetinTouch from "./GetinTouch";
import Link from "next/link";

function Navbar() {
  const [isActive, setIsActive] = useState(false);

  // Lock or unlock scroll based on isActive state
  useEffect(() => {
    if (isActive) {
      document.body.style.overflow = "hidden"; // Lock scroll
    } else {
      document.body.style.overflow = ""; // Unlock scroll
    }

    // Cleanup when component unmounts
    return () => {
      document.body.style.overflow = "";
    };
  }, [isActive]);

  const Dropodown = () => (
    <motion.ul
      initial={{ opacity: 0, y: -20 }}
      animate={{ opacity: 1, y: 0, transition: { duration: 0.5 } }}
      exit={{ opacity: 0, y: -20 }}
      // initial={{
      //   opacity: 0,
      //   scale: 6.5,
      //   rotate: -90,
      // }}
      // animate={{
      //   opacity: 1,
      //   scale: 1,
      //   rotate: 0,
      //   transition: { duration: 0.5 },
      // }}
      // exit={{ opacity: 0, scale: 0.8, rotate: 90 }}
      className="absolute w-screen h-screen -right-0  bg-white my-20 pb-8 z-50 "
    >
      <div className="flex flex-col gap-8 items-center justify-center py-16 pb-20">
        {/* <Link href="/productdesign" onClick={() => setIsActive(!isActive)}>
          <h1 className="text-5xl hover:underline">Product Design</h1>
        </Link> */}
        <Link href="/" onClick={() => setIsActive(!isActive)}>
          <h1 className="text-5xl hover:underline max-[370px]:text-4xl">
            Graphic design
          </h1>
        </Link>
        <Link href="/contact" onClick={() => setIsActive(!isActive)}>
          <h1 className="text-5xl hover:underline pb-10 max-[370px]:text-4xl">
            Contact
          </h1>
        </Link>
        {/* <div className="flex flex-row gap-5 justify-center items-center">
          <Link
            href="https://www.linkedin.com/in/miguelcasdesign/"
            target="_blank"
            rel="noopener"
          >
            <Image
              alt="Linked in "
              width={20}
              height={20}
              src="/linkedin-svgrepo-com.svg"
            />
          </Link>
          <Image
            alt="behance icon"
            width={30}
            height={30}
            src="/behance-svgrepo-com.svg"
          />
          <Image
            alt="vimeo icon"
            width={20}
            height={20}
            src="/vimeo-svgrepo-com.svg"
          />
        </div> */}
        <GetinTouch />
      </div>
    </motion.ul>
  );

  return (
    <div
      className={`flex flex-row justify-between py-10 px-[50px] items-center gap-6 relative ${
        isActive ? "pb-20" : ""
      }`}
    >
      <div className="flex gap-4">
        <div className="">
          <a
            href="/"
            className="gradient-text text-fluid text-start tracking-normal font-black text-transparent animate-gradient"
          >
            Miguel&apos;s Portfolio
          </a>
        </div>
      </div>
      {/* <div className="flex gap-5 items-center max-hamburger:hidden flex-wrap"></div> */}
      <div className="flex gap-5 items-center max-tablet:hidden">
        {/* <a href="/productdesign" className="text-nowrap">
          Product Design
        </a> */}
        <a href="/" className="text-nowrap">
          Graphic Design
        </a>
        <a href="/contact" className="text-nowrap">
          Contact
        </a>
        <a
          href="https://www.linkedin.com/in/miguelcasdesign/"
          target="_blank"
          rel="noopener"
          className="w-[25px] h-[25px]"
        >
          <Image
            src="/linkedin.png"
            height={25}
            width={25}
            alt="inked in"
            className=""
          />
        </a>
        <a
          href="https://www.behance.net/Miguelac44fc64"
          target="_blank"
          rel="noopener"
          className="w-[25px] h-[25px]"
        >
          <Image alt="behance" height={25} width={25} src="/behance.png" />
        </a>
        <a
          href="https://vimeo.com/user130033208"
          target="_blank"
          rel="noopener"
          className="w-[25px] h-[25px]"
        >
          <Image src="/vimeo.png" height={25} width={25} alt="vimeo" />
        </a>
        <a
          href="mailto:miguelcas.design@gmail.com?subject=Get in touch"
          target="_blank"
          className="bg-black border rounded-3xl text-white p-3 text-nowrap"
        >
          Get in touch!
        </a>
      </div>
      <div className="tablet:hidden">
        <button
          onClick={() => setIsActive(!isActive)}
          className={`hamburger hamburger--slider ${
            isActive ? "is-active" : ""
          }`}
          type="button"
        >
          <span className="hamburger-box">
            <span className="hamburger-inner"></span>
          </span>
        </button>{" "}
        <AnimatePresence>{isActive && <Dropodown />}</AnimatePresence>
        {/* <Image
          alt="hamburger"
          width={40}
          height={30}
          src="/hamburger.png"
        ></Image> */}
      </div>
    </div>
  );
}

export default Navbar;
