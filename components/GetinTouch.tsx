import Image from "next/image";
import React from "react";
import { Button } from "./ui/button";

const GetinTouch = () => {
  return (
    <div className="pt-16 pb-5 flex flex-col gap-5 justify-center items-center">
      {/* <a
        href="mailto:miguelcas.design@gmail.com?subject=Get in touch"
        target="_blank"
        className="bg-black border rounded-full text-white p-3 h-24 w-48 flex justify-center items-center text-xl shadow-getintouch"
      >
        Get in touch!
      </a> */}
      <div>
        <Button asChild>
          <a
            href="mailto:miguelcas.design@gmail.com?subject=Get in touch"
            target="_blank"
          >
            Get in touch
          </a>
        </Button>
      </div>
      <div className="flex gap-3 items-start justify-center">
        <a
          href="https://www.linkedin.com/in/miguelcasdesign/"
          target="_blank"
          rel="noopener"
        >
          <Image
            src="/linkedin-svgrepo-com.svg"
            // src="/linkedin.png"
            alt="linkedin"
            width={30}
            height={30}
          />
        </a>
        <a
          href="https://www.behance.net/Miguelac44fc64"
          target="_blank"
          rel="noopener"
        >
          <Image
            alt="behance icon"
            width={35}
            height={35}
            // src="/behance-svgrepo-com.svg"
            // src="/behance.png"
            src="/iconmonstr-behance-1.svg"
          />
        </a>
        <a
          href="https://vimeo.com/user130033208"
          target="_blank"
          rel="noopener"
        >
          <Image
            alt="vimeo icon"
            width={30}
            height={30}
            src="/vimeo-svgrepo-com.svg"
            // src="/vimeo.png"
          />
        </a>
      </div>
    </div>
  );
};

export default GetinTouch;
