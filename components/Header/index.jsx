"use client";
import React, { useState } from "react";
import Button from "./Button";
import styles from "./style.module.scss";
import Image from "next/image";
import "styles.css"; // Import your styles in the main JS file instead of using <link>

{
  /* <link href="dist/hamburgers.css" rel="stylesheet"></link>; */
}

const index = () => {
  // const [isActive, setIsActive] = useState(false);

  return (
    <div className={styles.header}>
      <div className="">
        <a href="/" className={styles.gradientText}>
          Miguel Castillo Design Portfolio
        </a>
      </div>
      <div className="flex gap-5 items-center max-hamburger:hidden flex-wrap">
        <a href="/productdesign" className="text-nowrap">
          Product Design
        </a>
        <a href="" className="text-nowrap">
          Graphic Design
        </a>
        <a href="/contact" className="text-nowrap">
          Contact
        </a>
      </div>
      <div className="flex gap-5 items-center max-hamburger:hidden">
        <a href="">Linkedin</a>
        <a href="">behance</a>
        <a href="">vimeo</a>
        <a
          href=""
          className="bg-black border rounded-3xl text-white p-3 text-nowrap"
        >
          Get in touch!
        </a>
      </div>
      <div className="hamburger:hidden">
        {/* <Button isActive={isActive} setIsActive={setIsActive} /> */}
        <button className="hamburger hamburger--slider is-active" type="button">
          <span className="hamburger-box">
            <span className="hamburger-inner"></span>
          </span>
        </button>{" "}
        {/* <Image
          alt="hamburger"
          width={40}
          height={30}
          src="/hamburger.png"
        ></Image> */}
      </div>
    </div>
  );
};

export default index;
