import React from "react";
import "styles.css"; // Import your styles in the main JS file instead of using <link>

{
  /* <link href="dist/hamburgers.css" rel="stylesheet"></link>; */
}

const index = (isActive, setIsActive) => {
  return (
    <div
      onClick={() => {
        setIsActive(!isActive);
      }}
    >
      <button
        className={`hamburger hamburger--slider ${isActive ? "is-active" : ""}`}
        type="button"
      >
        <span className="hamburger-box">
          <span className="hamburger-inner"></span>
        </span>
      </button>
    </div>
  );
};

export default index;
